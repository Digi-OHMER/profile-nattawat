import { Image, message } from "antd";
import { useState } from "react";
import { useTranslation } from "react-i18next";

const AboutPage = () => {
  const { t } = useTranslation();
  const [num, setNum] = useState(1);
  const [onRGB, setOnRGB] = useState(false);
  const hoverClass = onRGB ? "" : "2xl:hover:cursor-help";

  const success = () => {
    message.open({
      type: num <= 8 ? "warning" : onRGB ? "error" : "success",
      content:
        num <= 8
          ? `Count Rounds ${num}`
          : num <= 9
          ? `Count Rounds ${num} : RGB ON`
          : onRGB
          ? "RGB OFF"
          : "RGB ON",
      duration: num <= 7 ? 0.5 : num <= 8 ? 2 : 3,
    });
  };

  const handleClick = () => {
    if (num <= 9) {
      setNum(num + 1);
      if (num == 9) {
        setOnRGB(true);
      }
    } else {
      if (onRGB) {
        setOnRGB(false);
      } else {
        setOnRGB(true);
      }
    }
    success();
  };

  return (
    <div className="m-5 bg-white bg-opacity-30 backdrop-blur-sm shadow-2xl drop-shadow-2xl rounded-lg grid grid-cols-1 duration-1000 md:mx-36 lg:rounded-lg lg:shadow-none lg:drop-shadow-none lg:m-0 lg:mx-44">
      <div className="bg-white drop-shadow-2xl rounded-lg m-5 p-5 duration-1000 lg:rounded-none lg:m-0 lg:mx-10 2xl:mx-28">
        <div className={`${onRGB ? "textRGB" : ""}`}>
          <div className="text-3xl text-center font-bold lg:text-5xl 2xl:text-6xl">
            <div>
              <p className="lg:hidden"> {t("hello_im")} </p>
              <p className={`hidden lg:block ${hoverClass}`}>
                {t("hello_name")}
              </p>
            </div>
            <div className="underline decoration-from-font decoration-double underline-offset-8 lg:hidden">
              {t("name")}
            </div>
          </div>
          <div className="my-6 lg:hidden">
            <Image
              preview={false}
              src="https://drive.google.com/thumbnail?id=1PP8M-1WNhDXneWPjfxB1bIcvH_dGsLg0&sz=w3456"
              className="rounded-xl shadow-lg active:opacity-90 active:scale-105"
              onClick={handleClick}
            />
          </div>
          <div className="grid lg:grid-cols-5 lg:mt-6 lg:p-2">
            <div className="text-base lg:col-span-3 lg:p-3 lg:px-6 2xl:p-6 2xl:px-12 2xl:text-2xl">
              <div className="border-b-4">
                <div
                  className={`font-bold text-lg text-center 2xl:text-3xl lg:text-left ${hoverClass}`}
                >
                  {t("about_bold")}
                </div>
                <div className={`mb-2 2xl:mb-4 ${hoverClass}`}>
                  &emsp; {t("about_detail")}
                </div>
              </div>
              <div className="mt-6 border-b-4">
                <div
                  className={`font-bold text-lg text-center 2xl:text-3xl lg:text-left ${hoverClass}`}
                >
                  {t("Education_Background_bold")}
                </div>
                <div className={`mb-2 2xl:mb-4 ${hoverClass}`}>
                  &emsp; {t("Education_Background_detail_1")}
                  <br /> &emsp; {t("Education_Background_detail_2")} <br />
                  &emsp; {t("Education_Background_detail_3")}
                </div>
              </div>
              <div className="mt-6 border-b-4">
                <div
                  className={`font-bold text-lg text-center 2xl:text-3xl lg:text-left ${hoverClass}`}
                >
                  {t("hobby_bold")}
                </div>
                <div className={`mb-2 2xl:mb-4 ${hoverClass}`}>
                  &emsp; {t("hobby_detail")}
                </div>
              </div>
            </div>
            <div className="hidden lg:flex lg:flex-col lg:justify-center lg:items-center lg:col-span-2">
              <div className="lg:flex lg:flex-col lg:justify-center lg:items-center lg:border-4 lg:rounded-3xl lg:p-3 lg:border-gray-500">
                <div className="lg:flex lg:flex-col lg:justify-center lg:items-center lg:border-4 lg:border-dashed lg:rounded-3xl lg:p-3 lg:border-orange-400">
                  <Image
                    preview={false}
                    src="https://drive.google.com/thumbnail?id=1qghdou185tanPo7nYN5eN6NIwwUaji5P&sz=w3456"
                    className="rounded-xl shadow-lg duration-500 active:opacity-90 active:scale-105 lg:hover:cursor-pointer lg:hover:shadow-orange-600"
                    onClick={handleClick}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutPage;
