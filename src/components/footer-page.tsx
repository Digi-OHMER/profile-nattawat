import { Layout, Select, message } from "antd";
import { useTranslation } from "react-i18next";

const { Footer } = Layout;
const { Option } = Select;

const FooterPage = () => {
  const { t, i18n } = useTranslation();

  const handleChangeLanguage = (value: string) => {
    i18n.changeLanguage(value).then(() => {
      if (value == "th") {
        message.success(t("lang_th"));
      } else if (value == "en") {
        message.success(t("lang_en"));
      } else {
        message.success(t("lang_jp"));
      }
    });
  };

  return (
    <Footer className="flex justify-center items-center text-center bg-stone-600">
      Ant Design ©2024 Created by Digi-OHM
      <Select
        className="ml-2 md:ml-5"
        defaultValue={i18n.language}
        onChange={handleChangeLanguage}
      >
        <Option value="th">Thai</Option>
        <Option value="en">English</Option>
        <Option value="jp">Japan</Option>
      </Select>
    </Footer>
  );
};

export default FooterPage;
