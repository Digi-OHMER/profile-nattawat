import { Button, Image } from "antd";
import { useTranslation } from "react-i18next";

const HomePage = () => {
  const OuterEdge =
    "lg:items-center lg:justify-center lg:shadow-2xl lg:before:absolute lg:before:h-full lg:before:w-0 lg:before:rounded-lg lg:before:bg-white lg:before:duration-1000 lg:before:ease-out lg:hover:shadow-orange-600 lg:hover:before:h-full lg:hover:before:w-full lg:hover:cursor-move";
  const openResume = () => {
    window.open(
      "https://drive.google.com/file/d/1EpOzkZPhGsaXgFsPWrmHCYPtQkA2ImGu/view",
      "_blank"
    );
  };
  const openCV = () => {
    window.open(
      "https://drive.google.com/file/d/1yzhx633_JWD1JDrzEd0fmO9Tk-gph7XJ/view",
      "_blank"
    );
  };
  const openTranscript = () => {
    window.open(
      "https://drive.google.com/file/d/138X2jWTnEwpPhbR9AZ1BvjSMGyLfTFTP/view",
      "_blank"
    );
  };

  const { t } = useTranslation();

  return (
    <div
      className={`flex ${OuterEdge} bg-white bg-opacity-30 backdrop-blur-sm shadow-2xl m-5 drop-shadow-2xl rounded-lg flex justify-items-center grid p-5 duration-1000 md:mx-36 2xl:mx-36 2xl:ml-56 lg:grid-cols-7 lg:bg-opacity-0 lg:opacity-50 lg:hover:opacity-100 lg:text-white lg:hover:text-black lg:duration-500`}
    >
      <div className={`mx-10 flex items-center h-full lg:col-span-3`}>
        <Image
          // width={550}
          preview={false}
          src="https://drive.google.com/thumbnail?id=1yt9YoXSFYEOXzLxT4IEUhPYTB505RRLw&sz=w1000"
          className="rounded-full transform"
        />
      </div>
      <div className="text-3xl mt-5 bg-white drop-shadow-2xl rounded-lg p-5 grid grid-cols-1 lg:bg-transparent lg:shadow-none lg:rounded-none lg:justify-items-start lg:col-span-4 lg:mr-28 lg:p-0">
        <div className="font-bold text-center lg:flex flex-col lg:h-full lg:justify-end lg:text-left lg:text-5xl 2xl:text-6xl">
          <div>Website Portfolio</div>
          <div className="text-3xl lg:text-5xl 2xl:text-6xl">
            Nattawat Thanwiset
          </div>
        </div>
        <div className="mt-3 text-base 2xl:text-left 2xl:text-2xl">
          <div> &emsp; {t("home_detail")}</div>
          <div className="mt-5 flex justify-between lg:justify-start">
            <Button
              onClick={openResume}
              className="bg-sky-50 md:w-24 md:ml-10 lg:ml-0"
            >
              Resume
            </Button>
            <Button
              onClick={openCV}
              className="bg-yellow-50 w-16 md:w-24 lg:ml-5"
            >
              CV
            </Button>
            <Button
              onClick={openTranscript}
              className="bg-green-50 md:w-24 md:mr-10 lg:mr-0 lg:ml-5"
            >
              Transcript
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
