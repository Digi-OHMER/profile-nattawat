import { Image } from "antd";
import "../assets/button-welcome.css";

const WelcomePage = () => {
  const BackgroundImage =
    "https://drive.google.com/thumbnail?id=1JGWOUOfpDMx48vIg7eltSZsLmiaLe9VV&sz=w5184";
  return (
    <div
      className="flex-1 flex items-center bg-cover bg-no-repeat bg-center grid grid-cols-1"
      style={{ backgroundImage: `url(${BackgroundImage})` }}
    >
      <div className="flex justify-center h-full bg-black bg-opacity-20 grid grid-rows-2">
        <div className="flex justify-center content-end h-full grid grid-cols-3">
          <div className="flex justify-end items-center col-span-1 ml-12">
            <Image
              preview={false}
              src="https://drive.google.com/thumbnail?id=1jUomcyMluPrgaXlqj9NhvAkh4c9AcO74&sz=w156"
            ></Image>
          </div>
          <div className="flex justify-start items-center grid grid-rows-3 text-white font-bold col-span-2 ml-3">
            <div className="flex justify-left items-center h-full text-4xl md:text-6xl">
              The Profile
            </div>
            <div className="flex justify-left rounded-lg border-b-4 border-white md:border-4"></div>
            <div className="flex justify-left items-center h-full text-2xl md:text-4xl">
              Nattawat Thaniwset
            </div>
          </div>
        </div>

        <div className="flex justify-center content-start h-full">
          <div className="borders-div mt-10">
            <a href="/Home" className="flex justify-left md:justify-center">
              <button className="primary-button flex justify-center bg-zinc-700 border-2 text-white text-lg md:text-black md:border-none md:bg-opacity-60 md:text-4xl">
                welcome to my website
              </button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WelcomePage;
