// HeaderPage.tsx
import { Drawer, Menu, Image } from "antd";
import { Header } from "antd/es/layout/layout";
import { Link, useLocation } from "react-router-dom";
import { MenuOutlined } from "@ant-design/icons";
import { useState, useEffect } from "react";
import { LocationType } from "../interface/interface";
import { useTranslation } from "react-i18next";

const { Item } = Menu;

type HeaderPageProps = {
  setLocation: (location: LocationType) => void;
};

const HeaderPage: React.FC<HeaderPageProps> = ({ setLocation }) => {
  const [openMenu, setOpenMenu] = useState(false);
  const location = useLocation();

  useEffect(() => {
    setLocation({
      hash: location.hash,
      key: location.key,
      pathname: location.pathname,
      search: location.search,
      state: location.state,
    });
  }, [location, setLocation]);

  return (
    <Header className="bg-zinc-300 flex justify-between items-center grid grid-cols-4 lg:grid-cols-3">
      <div className="col-span-1 flex justify-left lg:justify-center">
        <Link to={"/"} className="flex justify-left lg:justify-center">
          <Image
            preview={false}
            width={90}
            src="https://drive.google.com/thumbnail?id=1Yd7u6QixRUidoIuf4D6mUxNHl4q7rwNu&sz=w2205"
          />
        </Link>
      </div>
      <div className="col-span-2 invisible lg:visible lg:flex lg:justify-left">
        <NavMenu />
      </div>
      <div className="col-span-1">
        <MenuOutlined
          className="flex justify-end lg:hidden"
          onClick={() => {
            setOpenMenu(true);
          }}
        />
        <Drawer
          width={250}
          placement="right"
          open={openMenu}
          onClick={() => {
            setOpenMenu(false);
          }}
          onClose={() => {
            setOpenMenu(false);
          }}
          closable={false}
        >
          <NavMenu isInline />
        </Drawer>
      </div>
    </Header>
  );
};

const NavMenu = ({ isInline = false }) => {
  const { t } = useTranslation();
  const navName = [
    [t("header_home"), "/Home"],
    [t("header_about"), "/About"],
    [t("header_skills"), "/Skills"],
    [t("header_Projects"), "/Projects"],
  ];

  return (
    <Menu theme="light" mode={isInline ? "inline" : "horizontal"}>
      {navName.map((item, index) => (
        <Item key={index} className="w-0 bg-zinc-300 lg:w-36 text-center">
          <Link to={item[1]} className="text-base hover:font-bold">
            {item[0]}
          </Link>
        </Item>
      ))}
    </Menu>
  );
};

export default HeaderPage;
