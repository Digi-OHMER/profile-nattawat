import { Image } from "antd";

const ErrorPage = () => {
  return (
    <div
      style={{ userSelect: "none" }}
      className="grid grid-cols-1 bg-white bg-opacity-30 backdrop-blur-sm shadow-2xl drop-shadow-2xl rounded-lg flex justify-items-center p-5 m-5 duration-1000 md:duration-500 md:mx-96 md:bg-opacity-0 md:backdrop-blur-none md:shadow-none"
    >
      <div className="grid grid-cols-1 flex md:mx-52 md:p-16 md:bg-white md:bg-opacity-30 md:backdrop-blur-sm md:shadow-2xl md:drop-shadow-2xl md:rounded-lg md:justify-items-center md:duration-1000">
        <Image
          preview={false}
          src="https://drive.google.com/thumbnail?id=1G1TMFbeyxTOg4uAl49v66yj1j_asWziy&sz=w2000"
          className="rounded-full"
        />
      </div>
    </div>
  );
};

export default ErrorPage;
