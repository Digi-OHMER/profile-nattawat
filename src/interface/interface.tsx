// interface.tsx

export type LocationType = {
  hash: string;
  key: string;
  pathname: string;
  search: string;
  state: string;
};
export const LocationVar = {
  hash: "",
  key: "",
  pathname: "",
  search: "",
  state: "",
};

export type Timeday = {
  time: string;
  day: string;
  week: string;
  month: string;
};
