import { Image, Modal } from "antd";
import { useState } from "react";
import { useTranslation } from "react-i18next";

const SkillsPage = () => {
  const { t, i18n } = useTranslation();
  const PythonLink =
    i18n.language == "th"
      ? "https://th.wikipedia.org/wiki/ภาษาไพทอน"
      : "https://en.wikipedia.org/wiki/Python_(programming_language)";
  const CPPLink =
    i18n.language == "th"
      ? "https://th.wikipedia.org/wiki/ภาษาซีพลัสพลัส"
      : "https://en.wikipedia.org/wiki/C%2B%2B";
  const JavaLink =
    i18n.language == "th"
      ? "https://th.wikipedia.org/wiki/ภาษาจาวา"
      : "https://en.wikipedia.org/wiki/Java_(programming_language)";
  const ReactLink =
    i18n.language == "th"
      ? "https://wiki.morange.co.th/React"
      : "https://en.wikipedia.org/wiki/React_(software)";

  const items = [
    {
      text: "Android Studio", //  1
      id_img: "1-bMkXz2XCrIiZXCqG3otqa_CL9IgWckr",
    },
    {
      text: "Ant Design",
      id_img: "1vFcdVKgFBCzjramU6f0kFQdDumW3mM0C",
    },
    {
      text: "Arduino IDE",
      id_img: "1H96dwGIY9QII2cvtejspBLVhdo8UtjBV",
    },
    {
      text: "Bard AI",
      id_img: "18E1WS91wVrOfFkMqI94LZjX3jpnhG6U2",
    },
    {
      text: "C#", //  5
      id_img: "1Co775myf9hOLu0ABG6ObhD9UjQdc3eEV",
    },
    {
      text: "Canva",
      id_img: "1D2SkZowtN3zJdQ9rhogIUQYmkOviao4z",
    },
    {
      text: "Chat GPT",
      id_img: "10UBva2OOaNoEZC1jQRW20z0fgkX82fpi",
    },
    {
      text: "CSS",
      id_img: "1nd41MD94ntKc0Hd3KypFDP82MmpM1m-V",
    },
    {
      text: "DB Browser SQLite",
      id_img: "1Uy22qDb6SOeWLaJV_DzAmazxmG3lbPa-",
    },
    {
      text: "Figma", //  10
      id_img: "1oOTTJjopDs-y1wdGKTrtASDULsFll185",
    },
    {
      text: "Flutter Basic",
      id_img: "1QPhWcipwF7NwiEqz7czObi0LoNHgCdXT",
    },
    {
      text: "Git",
      id_img: "1lULAwGmArAJ0WvC1Pdubi93T2w7W9SGL",
    },
    {
      text: "GitHub",
      id_img: "1BnQ9digeT0ZW7THe_woqmNHxSsNXiwJc",
    },
    {
      text: "GitLab",
      id_img: "1YHf4kaA9PRe2tTXSwL8JxaXYyDgTi2Ff",
    },
    {
      text: "HTML", //  15
      id_img: "15pRtX04p-nly1bVecxbTs0IuU-96l0ja",
    },
    {
      text: "JavaScript",
      id_img: "1RX1fGhhVi7gL-8L7xsw32hEh_wKxn7uT",
    },
    {
      text: "MySQL",
      id_img: "1ytltXmDKFoQBcMa2h-xHU8RSGlivR7KY",
    },
    {
      text: "Photoshop",
      id_img: "1eAnGRjzUQ4wnHucudW3dHtDUQugYWPhS",
    },
    {
      text: "PHP Basic",
      id_img: "1edaRpQgwu32EFg7SXgNHqyoh2nwh6_5r",
    },
    {
      text: "Sony Vegas", // 20
      id_img: "1787XqiN5MMoLaPjPanwZdhissMWDIzHB",
    },
    {
      text: "Tailwind",
      id_img: "19ZGz-WL0DKlx9bldFoVMkd2WZRisWAxW",
    },
    {
      text: "Think Speak",
      id_img: "15L_w4kGtowhcJdNeE7Ktg0sNa7BnUwXE",
    },
    {
      text: "Unity",
      id_img: "1KtiCQ4XWxtyLKhJCrC37QZ7qEedanmGu",
    },
    {
      text: "VS Code", // 24
      id_img: "16QURB7nYnd1n1K_JSlBv-8Rstzbfwhnp",
    },
  ];

  const [modalVisible, setModalVisible] = useState(false);
  const [modelLink, setModelLink] = useState("");
  const [modelName, setModelName] = useState("");

  const handleClick = (getLink: string, getName: string) => {
    setModelLink(getLink);
    setModelName(getName);
    setModalVisible(true);
  };

  const handleModalCancel = () => {
    setModalVisible(false);
  };

  const rowsMobile = [];
  for (let i = 0; i < items.length; i += 2) {
    const rowItems = items.slice(i, i + 2);
    rowsMobile.push(
      <div className="flex justify-center items-center" key={i}>
        {rowItems.map((item, index) => (
          <div
            key={index}
            className="flex justify-center items-center text-base ml-2 my-2"
          >
            <Image
              preview={true}
              src={`https://drive.google.com/thumbnail?id=${item.id_img}&sz=w250`}
              width="20px"
            />
            <p className="ml-0.5 mr-2">{item.text}</p>
          </div>
        ))}
      </div>
    );
  }

  const rowsDesktopLG = [];
  for (let i = 0; i < items.length; i += 3) {
    const rowItems = items.slice(i, i + 3);
    rowsDesktopLG.push(
      <div className="flex justify-center items-center" key={i}>
        {rowItems.map((item, index) => (
          <div
            key={index}
            className="flex justify-center items-center text-base ml-4 my-4"
          >
            <Image
              preview={true}
              src={`https://drive.google.com/thumbnail?id=${item.id_img}&sz=w500`}
              width="25px"
            />
            <p className="ml-1 mr-8">{item.text}</p>
          </div>
        ))}
      </div>
    );
  }

  const rowsDesktop2XL = [];
  for (let i = 0; i < items.length; i += 3) {
    const rowItems = items.slice(i, i + 3);
    rowsDesktop2XL.push(
      <div className="flex justify-center items-center" key={i}>
        {rowItems.map((item, index) => (
          <div
            key={index}
            className="flex justify-center items-center text-xl ml-8 my-4"
          >
            <Image
              preview={true}
              src={`https://drive.google.com/thumbnail?id=${item.id_img}&sz=w500`}
              width="30px"
            />
            <p className="ml-1 mr-16">{item.text}</p>
          </div>
        ))}
      </div>
    );
  }

  return (
    <div className="m-5 bg-white bg-opacity-30 backdrop-blur-sm shadow-2xl drop-shadow-2xl rounded-lg grid grid-cols-1 duration-1000 md:mx-36 lg:m-0 lg:mx-44 lg:rounded-lg lg:shadow-none lg:drop-shadow-none">
      <div className="drop-shadow-2xl rounded-lg m-5 lg:m-0 lg:rounded-none lg:bg-white lg:mx-10 2xl:mx-28">
        <div className="text-2xl font-bold text-center bg-white p-2 rounded-lg lg:rounded-none lg:bg-inherit lg:text-5xl 2xl:text-6xl lg:mt-10">
          {t("ability_from_experience")}
          <br></br>
          {t("Study_Work")}
        </div>
        <div className="mt-5 mb-2 mx-16 p-3 bg-white rounded-t-lg 2xl:rounded-none 2xl:bg-inherit 2xl:mb-10">
          <div className="text-2xl font-bold text-center border-b-4 lg:border-none lg:text-5xl 2xl:text-6xl">
            {t("Skills")}
          </div>
        </div>
        <div className="mb-5 lg:mx-16 2xl:mx-28 lg:mb-10">
          <div
            onClick={() => handleClick(PythonLink, "Python")}
            className="grid grid-cols-1 bg-white rounded-lg drop-shadow-2xl shadow-lg duration-1000 lg:grid-cols-5 lg:flex-col lg:justify-center lg:items-center lg:hover:cursor-pointer lg:hover:shadow-orange-600 lg:p-5 2xl:p-10"
          >
            <div className="flex justify-center my-4 mx-20 lg:col-span-2">
              <Image
                preview={false}
                src="https://drive.google.com/thumbnail?id=1yulBGLWruxCZy8t0rZJKg0tpNUqwpam1&sz=w1200"
              />
            </div>
            <div className="px-4 pb-4 lg:col-span-3">
              <div className="text-lg font-bold text-center lg:text-left lg:mb-2 lg:text-2xl 2xl:text-3xl">
                Python
              </div>
              <div className="text-base 2xl:text-2xl">
                &nbsp; &nbsp; &nbsp; {t("python_detail_1")}
                <br />
                <br />
                &nbsp; &nbsp; &nbsp;
                {t("python_detail_2")}
              </div>
            </div>
          </div>
        </div>
        <div className="mb-5 lg:mx-16 2xl:mx-28 lg:mb-10">
          <div
            onClick={() => handleClick(CPPLink, "C++")}
            className="grid grid-cols-1 bg-white rounded-lg drop-shadow-2xl shadow-lg duration-1000 lg:grid-cols-5 lg:flex-col lg:justify-center lg:items-center lg:hover:cursor-pointer lg:hover:shadow-orange-600 lg:p-5 2xl:p-10"
          >
            <div className="flex justify-center my-4 mx-20 lg:col-span-2">
              <Image
                preview={false}
                src="https://drive.google.com/thumbnail?id=1iCDTUmeT1FbdIcND_dPhzEW63RrobxQY&sz=w1200"
              />
            </div>
            <div className="px-4 pb-4 lg:col-span-3">
              <div className="text-lg font-bold text-center lg:text-left lg:mb-2 lg:text-2xl 2xl:text-3xl">
                C++
              </div>
              <div className="text-base 2xl:text-2xl">
                &nbsp; &nbsp; &nbsp; {t("cpp_detail_1")}
                <br />
                <br />
                &nbsp; &nbsp; &nbsp;
                {t("cpp_detail_2")}
              </div>
            </div>
          </div>
        </div>
        <div className="mb-5 lg:mx-16 2xl:mx-28 lg:mb-10">
          <div
            onClick={() => handleClick(JavaLink, "Java")}
            className="grid grid-cols-1 bg-white rounded-lg drop-shadow-2xl shadow-lg duration-1000 lg:grid-cols-5 lg:flex-col lg:justify-center lg:items-center lg:hover:cursor-pointer lg:hover:shadow-orange-600 lg:p-5 2xl:p-10"
          >
            <div className="flex justify-center my-4 mx-20 lg:col-span-2">
              <Image
                preview={false}
                src="https://drive.google.com/thumbnail?id=10xmJ69LJRb3ulqRJee_iyJXbfWA5IUd7&sz=w1200"
              />
            </div>
            <div className="px-4 pb-4 lg:col-span-3">
              <div className="text-lg font-bold text-center lg:text-left lg:mb-2 lg:text-2xl 2xl:text-3xl">
                Java
              </div>
              <div className="text-base 2xl:text-2xl">
                &nbsp; &nbsp; &nbsp; {t("java_detail")}
              </div>
            </div>
          </div>
        </div>
        <div className="mb-5 lg:mx-16 2xl:mx-28 lg:mb-10">
          <div
            onClick={() => handleClick(ReactLink, "React")}
            className="grid grid-cols-1 bg-white rounded-lg drop-shadow-2xl shadow-lg duration-1000 lg:grid-cols-5 lg:flex-col lg:justify-center lg:items-center lg:hover:cursor-pointer lg:hover:shadow-orange-600 lg:p-5 2xl:p-10"
          >
            <div className="flex justify-center my-4 mx-20 lg:col-span-2">
              <Image
                preview={false}
                src="https://drive.google.com/thumbnail?id=1-Sw4eRUQH92RQGJoWjE3tW8g7di_Hz6j&sz=w1200"
              />
            </div>
            <div className="px-4 pb-4 lg:col-span-3">
              <div className="text-lg font-bold text-center lg:text-left lg:mb-2 lg:text-2xl 2xl:text-3xl">
                React
              </div>
              <div className="text-base 2xl:text-2xl">
                &nbsp; &nbsp; &nbsp; {t("react_detail_1")}
                <br />
                <br />
                &nbsp; &nbsp; &nbsp; {t("react_detail_2")}
              </div>
            </div>
          </div>
        </div>
        <div className="mb-5 lg:mx-16 2xl:mx-28 lg:mb-10">
          <div className="bg-white rounded-lg drop-shadow-2xl shadow-lg duration-1000 lg:grid-cols-5 lg:flex-col lg:justify-center lg:items-center lg:hover:cursor-pointer lg:hover:shadow-orange-600 lg:p-5 2xl:p-10">
            <div className="px-4 pb-4">
              <div className="text-center pt-5 text-lg font-bold text-center lg:text-2xl 2xl:text-3xl lg:mb-2 lg:pt-0">
                {t("others")}
              </div>
              <div className="flex flex-col justify-center items-center my-3 lg:my-5">
                <div className="lg:hidden">{rowsMobile}</div>
                <div className="hidden lg:block 2xl:hidden">
                  {rowsDesktopLG}
                </div>
                <div className="hidden 2xl:block">{rowsDesktop2XL}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        title={`Popup website  ${modelName}`}
        visible={modalVisible}
        onCancel={handleModalCancel}
        centered
        footer={null}
        width={1200}
        className="m-5"
      >
        <div className="bg-stone-200 px-2 py-2 2xl:px-10 2xl:py-7">
          <iframe width="100%" height="700" src={modelLink}></iframe>
        </div>
      </Modal>
    </div>
  );
};

export default SkillsPage;
