import { Image, Spin, Button } from "antd";
import { useState } from "react";
import {
  GitlabOutlined,
  DownCircleOutlined,
  UpCircleOutlined,
} from "@ant-design/icons";
import { useTranslation } from "react-i18next";

const ProkectsPage = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState({
    pdf_iot: false,
    vdo_iot: false,
    img_android: false,
    pdf_android: false,
    pdf_ai: false,
    vdo_ai: false,
    pdf_dt: false,
    slide_dt: false,
    img_cdg: false,
    pdf_cdg: false,
    img_yim: false,
    pdf_yim: false,
    logo_iot: false,
    logo_android: false,
    logo_ai: false,
    logo_dt: false,
    logo_web: false,
    logo_cdg: false,
    logo_yim: false,
  });

  const handleLoad = (section: string) => {
    setLoading((prevState) => ({
      ...prevState,
      [section]: true,
    }));
  };

  const [selectBox, setSelectBox] = useState("");
  const [onBox, setOnBox] = useState({
    on_iot: false,
    on_android: false,
    on_ai: false,
    on_dt: false,
    on_cdg: false,
    on_yim: false,
  });

  const handleClick = (select: string) => {
    let newSelect = select;
    if (selectBox == select) {
      newSelect = "";
      setOnBox((prevOnBox) => ({
        ...prevOnBox,
        [select]: false,
      }));
    } else {
      setOnBox({
        on_iot: false,
        on_android: false,
        on_ai: false,
        on_dt: false,
        on_cdg: false,
        on_yim: false,
      });
      setOnBox((prevOnBox) => ({
        ...prevOnBox,
        [select]: true,
      }));
    }
    setSelectBox(newSelect);
  };

  const windowOpen = (
    <div className="flex justify-end items-end">
      <DownCircleOutlined className="flex justify-end items-end text-xl lg:text-2xl 2xl:text-4xl mt-4 mr-6" />
    </div>
  );

  const windowClose = (
    <div className="flex justify-end items-end">
      <UpCircleOutlined className="flex justify-end items-end text-xl lg:text-2xl 2xl:text-4xl mt-4 mr-6" />
    </div>
  );

  return (
    <div className="m-5 bg-white bg-opacity-0 backdrop-blur-sm shadow-2xl drop-shadow-2xl rounded-lg grid grid-cols-1 duration-1000 md:mx-36 lg:m-0 lg:mx-44 lg:rounded-lg lg:shadow-none lg:drop-shadow-none">
      <div className="drop-shadow-2xl rounded-lg m-5 lg:m-0 lg:mx-10 lg:p-10 2xl:mx-28 lg:rounded-lg lg:bg-zinc-100 lg:bg-opacity-20 lg:my-10 2xl:p-10">
        <div className="text-2xl mb-5 mx-16 font-bold text-center bg-white p-2 rounded-lg lg:text-5xl 2xl:text-6xl lg:mx-72 2xl:mx-80 lg:my-10">
          <p className="lg:mb-3">{t("header_Projects")}</p>
        </div>
        <div className="bg-white rounded-lg drop-shadow-2xl mb-10 shadow-lg duration-500 lg:hover:shadow-orange-600 lg:hover:cursor-pointer">
          <div onClick={() => handleClick("on_iot")}>
            {!onBox.on_iot ? windowOpen : windowClose}
            <div className="grid grid-cols-1 lg:grid-cols-5 lg:px-10 lg:pb-10 lg:flex-col lg:justify-center lg:items-center">
              <div className="flex justify-center my-4 mx-20 lg:col-span-2">
                {!loading.logo_iot && (
                  <div className="flex justify-center items-center h-64">
                    <Spin size="large" />
                  </div>
                )}
                <Image
                  preview={false}
                  src="https://drive.google.com/thumbnail?id=1Rk4kO-nZZBOb8vZ3ZafIDDLeZD-L91mj&sz=w1000"
                  onLoad={() => handleLoad("logo_iot")}
                  style={{
                    display: loading.logo_iot ? "block" : "none",
                  }}
                />
              </div>
              <div className="px-4 pb-4 lg:col-span-3">
                <div className="text-lg font-bold text-center lg:text-left lg:text-3xl lg:mb-2">
                  Internet of Things
                </div>
                <div className="text-base 2xl:text-2xl">
                  &nbsp; &nbsp; &nbsp; {t("details_iot")}
                </div>
              </div>
            </div>
          </div>

          {onBox.on_iot && (
            <>
              <div className="border-b-2 lg:col-span-5 lg:mb-5"></div>
              <div className="grid lg:grid-cols-2 lg:col-span-5">
                <div className="p-5 lg:p-10">
                  {!loading.pdf_iot && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    src="https://drive.google.com/file/d/10jJzNWnTkzrm-5djdN2m_115lDrw0JPY/preview"
                    width="100%"
                    height="320px"
                    allow="autoplay"
                    onLoad={() => handleLoad("pdf_iot")}
                    style={{ display: loading.pdf_iot ? "block" : "none" }}
                  ></iframe>
                </div>
                <div className="p-5 lg:p-10">
                  {!loading.vdo_iot && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    width="100%"
                    height="320"
                    src="https://www.youtube.com/embed/QRIGLz19zrw"
                    title="Smart Trash"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    referrerPolicy="strict-origin-when-cross-origin"
                    allowFullScreen
                    onLoad={() => handleLoad("vdo_iot")}
                    style={{ display: loading.vdo_iot ? "block" : "none" }}
                  ></iframe>
                </div>
              </div>
            </>
          )}
        </div>

        <div className="bg-white rounded-lg drop-shadow-2xl mb-10 shadow-lg duration-500 lg:hover:shadow-orange-600 lg:hover:cursor-pointer">
          <div onClick={() => handleClick("on_android")}>
            {!onBox.on_android ? windowOpen : windowClose}
            <div className="grid grid-cols-1 lg:grid-cols-5 lg:px-10 lg:pb-10 lg:flex-col lg:justify-center lg:items-center">
              <div className="flex justify-center my-4 mx-20 lg:col-span-2">
                {!loading.logo_android && (
                  <div className="flex justify-center items-center h-64">
                    <Spin size="large" />
                  </div>
                )}
                <Image
                  preview={false}
                  src="https://drive.google.com/thumbnail?id=1liTLFlgfZ-u7jepfQJJeZFoaB3gpC3lw&sz=w1000"
                  onLoad={() => handleLoad("logo_android")}
                  style={{
                    display: loading.logo_android ? "block" : "none",
                  }}
                />
              </div>
              <div className="px-4 pb-4 lg:col-span-3">
                <div className="text-lg font-bold text-center lg:text-left lg:text-3xl lg:mb-2">
                  Android Application Development
                </div>
                <div className="text-base 2xl:text-2xl">
                  &nbsp; &nbsp; &nbsp; {t("details_android")}
                </div>
              </div>
            </div>
          </div>

          {onBox.on_android && (
            <>
              <div className="border-b-2 lg:col-span-5 lg:mb-5"></div>
              <div className="grid lg:grid-cols-2 lg:col-span-5">
                <div className="p-5 lg:p-10">
                  <div style={{ height: "320px", overflow: "auto" }}>
                    {!loading.img_android && (
                      <div className="flex justify-center items-center h-64">
                        <Spin size="large" />
                      </div>
                    )}
                    <Image
                      src="https://drive.google.com/thumbnail?id=1gWg31VKPdy1s3-JycYOhOFgHuRnIy3ur&sz=w4240"
                      onLoad={() => handleLoad("img_android")}
                      style={{
                        display: loading.img_android ? "block" : "none",
                      }}
                    />
                  </div>
                </div>
                <div className="p-5 lg:p-10">
                  {!loading.pdf_android && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    width="100%"
                    height="320"
                    src="https://drive.google.com/file/d/1Z2ZBjuGZtypIGx-288ZVEod_w8TEyn_z/preview"
                    title="Group 1_Spam Detection_PROJECT"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    referrerPolicy="strict-origin-when-cross-origin"
                    allowFullScreen
                    onLoad={() => handleLoad("pdf_android")}
                    style={{ display: loading.pdf_android ? "block" : "none" }}
                  ></iframe>
                </div>
              </div>
            </>
          )}
        </div>

        <div className="bg-white rounded-lg drop-shadow-2xl mb-10 shadow-lg duration-500 lg:hover:shadow-orange-600 lg:hover:cursor-pointer">
          <div onClick={() => handleClick("on_ai")}>
            {!onBox.on_ai ? windowOpen : windowClose}
            <div className="grid grid-cols-1 lg:grid-cols-5 lg:px-10 lg:pb-10 lg:flex-col lg:justify-center lg:items-center">
              <div className="flex justify-center my-4 mx-20 lg:col-span-2">
                {!loading.logo_ai && (
                  <div className="flex justify-center items-center h-64">
                    <Spin size="large" />
                  </div>
                )}
                <Image
                  preview={false}
                  src="https://drive.google.com/thumbnail?id=10U9V3Tk4HGsJoebtAtm_tWtuPfrlSclC&sz=w1000"
                  onLoad={() => handleLoad("logo_ai")}
                  style={{
                    display: loading.logo_ai ? "block" : "none",
                  }}
                />
              </div>
              <div className="px-4 pb-4 lg:col-span-3">
                <div className="text-lg font-bold text-center lg:text-left lg:text-3xl lg:mb-2">
                  Artificial Intelligence for Data Analytics
                </div>
                <div className="text-base 2xl:text-2xl">
                  &nbsp; &nbsp; &nbsp; {t("details_ai")}
                </div>
              </div>
            </div>
          </div>

          {onBox.on_ai && (
            <>
              <div className="border-b-2 lg:col-span-5 lg:mb-5"></div>
              <div className="grid lg:grid-cols-2 lg:col-span-5">
                <div className="p-5 lg:p-10">
                  {!loading.pdf_ai && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    src="https://drive.google.com/file/d/1Afl57a8ZEX_RkEcnxuCclKnO1Zk3obTt/preview"
                    width="100%"
                    height="320px"
                    allow="autoplay"
                    onLoad={() => handleLoad("pdf_ai")}
                    style={{ display: loading.pdf_ai ? "block" : "none" }}
                  ></iframe>
                </div>
                <div className="p-5 lg:p-10">
                  {!loading.vdo_ai && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    width="100%"
                    height="320"
                    src="https://www.youtube.com/embed/zlhGlJZLcFM"
                    title="Group 1_Spam Detection_PROJECT"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    referrerPolicy="strict-origin-when-cross-origin"
                    allowFullScreen
                    onLoad={() => handleLoad("vdo_ai")}
                    style={{ display: loading.vdo_ai ? "block" : "none" }}
                  ></iframe>
                </div>
              </div>
            </>
          )}
        </div>

        <div className="bg-white rounded-lg drop-shadow-2xl mb-10 shadow-lg duration-500 lg:hover:shadow-orange-600 lg:hover:cursor-pointer">
          <div onClick={() => handleClick("on_dt")}>
            {!onBox.on_dt ? windowOpen : windowClose}
            <div className="grid grid-cols-1 lg:grid-cols-5 lg:px-10 lg:pb-10 lg:flex-col lg:justify-center lg:items-center">
              <div className="flex justify-center my-4 mx-20 lg:col-span-2">
                {!loading.logo_dt && (
                  <div className="flex justify-center items-center h-64">
                    <Spin size="large" />
                  </div>
                )}
                <Image
                  preview={false}
                  src="https://drive.google.com/thumbnail?id=1VZ3W2Koe6xEY4R9MeAXBgqubfHQo3TuR&sz=w1000"
                  onLoad={() => handleLoad("logo_dt")}
                  style={{
                    display: loading.logo_dt ? "block" : "none",
                  }}
                />
              </div>
              <div className="px-4 pb-4 lg:col-span-3">
                <div className="text-lg font-bold text-center lg:text-left lg:text-3xl lg:mb-2">
                  Project in Digital Technology
                </div>
                <div className="text-base 2xl:text-2xl">
                  &nbsp; &nbsp; &nbsp; {t("details_dt")}
                </div>
              </div>
            </div>
          </div>

          {onBox.on_dt && (
            <>
              <div className="border-b-2 lg:col-span-5 lg:mb-5"></div>
              <div className="grid lg:grid-cols-2 lg:col-span-5">
                <div className="p-5 lg:p-10">
                  {!loading.pdf_dt && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    src="https://drive.google.com/file/d/1LKO16o7so531v0RdQ3YhDi-0sgpYB_n6/preview"
                    width="100%"
                    height="320px"
                    allow="autoplay"
                    onLoad={() => handleLoad("pdf_dt")}
                    style={{ display: loading.pdf_dt ? "block" : "none" }}
                  ></iframe>
                </div>
                <div className="p-5 lg:p-10">
                  {!loading.slide_dt && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    src="https://drive.google.com/file/d/1e4zUj98pGSE4rWogcw9H1OI4EODHWOfC/preview"
                    width="100%"
                    height="320px"
                    allow="autoplay"
                    onLoad={() => handleLoad("slide_dt")}
                    style={{ display: loading.slide_dt ? "block" : "none" }}
                  ></iframe>
                </div>
              </div>
            </>
          )}
        </div>

        <div className="grid grid-cols-1 bg-white rounded-lg drop-shadow-2xl shadow-lg duration-500 mb-10 lg:grid-cols-5 lg:p-10 lg:flex-col lg:justify-center lg:items-center">
          <div className="flex justify-center my-4 mx-20 lg:col-span-2">
            {!loading.logo_web && (
              <div className="flex justify-center items-center h-64">
                <Spin size="large" />
              </div>
            )}
            <Image
              preview={false}
              src="https://drive.google.com/thumbnail?id=1vrRFU4UU-xsbapwhSYIOXLRt5ne5nRIq&sz=w1000"
              onLoad={() => handleLoad("logo_web")}
              style={{
                display: loading.logo_web ? "block" : "none",
              }}
            />
          </div>
          <div className="px-4 pb-4 lg:col-span-3">
            <div className="text-lg font-bold text-center lg:text-left lg:text-3xl lg:mb-2">
              My website profile
            </div>
            <div className="text-base 2xl:text-2xl">
              &nbsp; &nbsp; &nbsp; {t("details_web")}
            </div>
            <div className="text-base mt-3 lg:mt-5">
              <div className="flex justify-center items-center lg:justify-start">
                <Button
                  href="https://gitlab.com/Digi-OHMER/profile-nattawat/"
                  target="_blank"
                  className="flex justify-center items-center 2xl:justify-start"
                >
                  <GitlabOutlined style={{ fontSize: "22px", color: "#08c" }} />
                  GitLab
                </Button>
              </div>
            </div>
          </div>
        </div>

        <div className="bg-white rounded-lg drop-shadow-2xl mb-10 shadow-lg duration-500 lg:hover:shadow-orange-600 lg:hover:cursor-pointer">
          <div onClick={() => handleClick("on_cdg")}>
            {!onBox.on_cdg ? windowOpen : windowClose}
            <div className="grid grid-cols-1 lg:grid-cols-5 lg:px-10 lg:pb-10 lg:flex-col lg:justify-center lg:items-center">
              <div className="flex justify-center my-4 mx-20 lg:col-span-2">
                {!loading.logo_cdg && (
                  <div className="flex justify-center items-center h-64">
                    <Spin size="large" />
                  </div>
                )}
                <Image
                  preview={false}
                  src="https://drive.google.com/thumbnail?id=1-dPlkBO4uB8fRwsCjbAoQPnIyaMPIhtC&sz=w1000"
                  onLoad={() => handleLoad("logo_cdg")}
                  style={{
                    display: loading.logo_cdg ? "block" : "none",
                  }}
                />
              </div>
              <div className="px-4 pb-4 lg:col-span-3">
                <div className="text-lg font-bold text-center lg:text-left lg:text-3xl lg:mb-2">
                  CDG Group
                </div>
                <div className="text-base 2xl:text-2xl">
                  &nbsp; &nbsp; &nbsp; {t("details_cdg")}
                </div>
              </div>
            </div>
          </div>

          {onBox.on_cdg && (
            <>
              <div className="border-b-2 lg:col-span-5 lg:mb-5"></div>
              <div className="grid lg:grid-cols-2 lg:col-span-5">
                <div className="p-5 lg:p-10">
                  <div style={{ height: "320px", overflow: "auto" }}>
                    {!loading.img_cdg && (
                      <div className="flex justify-center items-center h-64">
                        <Spin size="large" />
                      </div>
                    )}
                    <Image
                      src="https://drive.google.com/thumbnail?id=10JKoM9v4m3dNTro2if-25_eBA9g80dlb&sz=w1653"
                      onLoad={() => handleLoad("img_cdg")}
                      style={{
                        display: loading.img_cdg ? "block" : "none",
                      }}
                    />
                  </div>
                </div>
                <div className="p-5 lg:p-10">
                  {!loading.pdf_cdg && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    src="https://drive.google.com/file/d/1NU8pGK-O7Q9PYHoqrdzqDBoFiCNcHIx8/preview"
                    width="100%"
                    height="320px"
                    allow="autoplay"
                    onLoad={() => handleLoad("pdf_cdg")}
                    style={{ display: loading.pdf_cdg ? "block" : "none" }}
                  ></iframe>
                </div>
              </div>
            </>
          )}
        </div>

        <div className="bg-white rounded-lg drop-shadow-2xl mb-5 shadow-lg duration-500 lg:hover:shadow-orange-600">
          <div onClick={() => handleClick("on_yim")}>
            {!onBox.on_yim ? windowOpen : windowClose}
            <div className="grid grid-cols-1 lg:grid-cols-5 lg:px-10 lg:pb-10 lg:flex-col lg:justify-center lg:items-center lg:hover:cursor-pointer">
              <div className="flex justify-center my-4 mx-20 lg:col-span-2">
                {!loading.logo_yim && (
                  <div className="flex justify-center items-center h-64">
                    <Spin size="large" />
                  </div>
                )}
                <Image
                  preview={false}
                  src="https://drive.google.com/thumbnail?id=1zzPnULCFchZj5P6UBrQY78McJcKAJ4bw&sz=w1000"
                  onLoad={() => handleLoad("logo_yim")}
                  style={{
                    display: loading.logo_yim ? "block" : "none",
                  }}
                />
              </div>
              <div className="px-4 pb-4 lg:col-span-3">
                <div className="text-lg font-bold text-center lg:text-left lg:text-3xl lg:mb-2">
                  Y.I.M. Corporation
                </div>
                <div className="text-base 2xl:text-2xl">
                  &nbsp; &nbsp; &nbsp; {t("details_yim")}
                </div>
              </div>
            </div>
          </div>

          {onBox.on_yim && (
            <>
              <div className="border-b-2 lg:col-span-5 lg:mb-5"></div>
              <div className="grid lg:grid-cols-2 lg:col-span-5">
                <div className="p-5 lg:p-10">
                  <div style={{ height: "320px", overflow: "auto" }}>
                    {!loading.img_yim && (
                      <div className="flex justify-center items-center h-64">
                        <Spin size="large" />
                      </div>
                    )}
                    <Image
                      src="https://drive.google.com/thumbnail?id=119Z1D1GCDXEL5mrk7jsr9Xexf3lxpUsP&sz=w2339"
                      onLoad={() => handleLoad("img_yim")}
                      style={{
                        display: loading.img_yim ? "block" : "none",
                      }}
                    />
                  </div>
                </div>
                <div className="p-5 lg:p-10">
                  {!loading.pdf_yim && (
                    <div className="flex justify-center items-center h-64">
                      <Spin size="large" />
                    </div>
                  )}
                  <iframe
                    src="https://drive.google.com/file/d/1RA4Ckpqppeo_fO4AtoyQDBMLDpL48CLn/preview"
                    width="100%"
                    height="320px"
                    allow="autoplay"
                    onLoad={() => handleLoad("pdf_yim")}
                    style={{ display: loading.pdf_yim ? "block" : "none" }}
                  ></iframe>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default ProkectsPage;
