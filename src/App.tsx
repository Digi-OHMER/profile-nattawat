// App.tsx
import { useState } from "react";
import { Layout } from "antd";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import HeaderPage from "./components/header-page";
import FooterPage from "./components/footer-page";
import WelcomePage from "./pages/welcome-page";
import HomePage from "./pages/home-page";
import AboutPage from "./pages/about-page";
import SkillsPage from "./pages/skills-page";
import ProjectsPage from "./pages/projects-page";
import ErrorPage from "./pages/error-page";
import { LocationType, LocationVar } from "./interface/interface";
import "./assets/text-rgb.css";

const { Content } = Layout;

const App = () => {
  const [location, setLocation] = useState<LocationType>(LocationVar);

  return (
    <Router>
      <Layout className="min-h-screen flex flex-col">
        {location.pathname !== "/" && (
          <div className="min-h-screen flex flex-col">
            <HeaderPage setLocation={setLocation} />
            <Content
              className="flex-1 flex items-center bg-cover bg-no-repeat bg-center"
              style={{
                backgroundImage: `url(https://drive.google.com/thumbnail?id=1JGWOUOfpDMx48vIg7eltSZsLmiaLe9VV&sz=w4923)`,
              }}
            >
              <Routes>
                <Route path="/Home" element={<HomePage />} />
                <Route path="/About" element={<AboutPage />} />
                <Route path="/Skills" element={<SkillsPage />} />
                <Route path="/Projects" element={<ProjectsPage />} />
                <Route path="*" element={<ErrorPage />} />
              </Routes>
            </Content>
            <FooterPage />
          </div>
        )}
        <Routes>
          <Route path="/" element={<WelcomePage />} />
        </Routes>
      </Layout>
    </Router>
  );
};

export default App;
